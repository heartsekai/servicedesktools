﻿using System.Collections.Generic;
using ServiceDeskTools.Common;
using ServiceDeskTools.Suva;

namespace ServiceDeskTools
{
    public class GetLogonTraceUseCase
    {
        public List<LogonTrace> GetLogonTracesFromUser(string path, string username)
        {
            
            var files = new LogonTraceFinder(path);

            return files.GetLogonTracesFrom(username);

        }

        public List<LogonTrace> GetLogonTracesFromComputer(string path, string computerName)
        {
            var files = new LogonTraceFinder(path);
            var traces = files.GetLogonTracesFrom(null);
            List<LogonTrace> result = new List<LogonTrace>();
            foreach (var trace in traces)
            {
                if (trace.ComputerName == computerName)
                {
                   result.Add(trace); 
                } 
            }

            return result;
        }
    }
}