﻿using System.Collections.Generic;
using ServiceDeskTools.Suva;

namespace ServiceDeskTools.Common
{
    public interface IFileFinder
    {
        List<LogonTrace> GetLogonTracesFrom(string userName);
    }
}