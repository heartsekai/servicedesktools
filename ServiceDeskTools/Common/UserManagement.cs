﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;

namespace ServiceDeskTools.Common
{
    public class UserManagement
    {
        public bool AddUserToLocalGroup(string userName, string groupName, string computerName)
        {
            var groupContext = new PrincipalContext(ContextType.Machine,computerName);
            var group = GroupPrincipal.FindByIdentity(groupContext, groupName);

            if (group == null)
            {
                throw new Exception($"{groupName} not found in {groupContext.ContextType} on {computerName}");
            }
            
            var userContext = new PrincipalContext(ContextType.Domain);
            var user = UserPrincipal.FindByIdentity(userContext, userName);
            
            if (user == null)
                throw new Exception($"{userName} not found in {userContext.ConnectedServer}");

            if (group.Members.Contains(user)) return true;
            
            group.Members.Add(user);
            group.Save();

            return true;
        }

        public bool DeleteProfile(string userName, string computerName)
        {
            // Source: https://stackoverflow.com/questions/55296905/removing-a-windows-user-using-c-sharp-remotely
            try
            {
                ManagementScope scope = new ManagementScope($"\\\\{computerName}\\root\\cimv2");
                
                // Connect
                scope.Connect();
                
                SelectQuery query = new SelectQuery("Win32_UserProfile");
                var searcher = new ManagementObjectSearcher(scope, query);

                foreach (ManagementObject managementObject in searcher.Get())
                {
                    string normalizedUsername = managementObject["LocalPath"].ToString().Split('\\').Last();
                    if (normalizedUsername == userName)
                    {
                        managementObject.Delete();
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Failed to delete the profile, {userName} on {computerName}.");
            }

            // no profiles found
            return false;

        }
        public bool RemoveUserToLocalGroup(string userName, string groupName, string computerName)
        {
            var groupContext = new PrincipalContext(ContextType.Machine,computerName);
            var group = GroupPrincipal.FindByIdentity(groupContext, groupName);

            if (group == null)
            {
                throw new Exception($"{groupName} not found in {groupContext.ContextType} on {computerName}");
            }
            
            var userContext = new PrincipalContext(ContextType.Domain);
            var user = UserPrincipal.FindByIdentity(userContext, userName);
            
            if (user == null)
                throw new Exception($"{userName} not found in {userContext.ConnectedServer}");

            if (!group.Members.Contains(user)) return false;
            group.Members.Remove(user);
            group.Save();
            return true;


        }
    }
}