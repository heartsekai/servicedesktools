﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ServiceDeskTools.Suva;

namespace ServiceDeskTools.Common
{
    public class LogonTraceFinder : IFileFinder
    {
        private string _folder;

        public LogonTraceFinder(string logonTraceFolder)
        {
            _folder = logonTraceFolder;
        }

        public List<LogonTrace> GetLogonTracesFrom(string userName)
        {
            var directory = new DirectoryInfo(_folder);
            var files = directory.GetFiles($"{userName}*");

            var result = files.Select(file => new LogonTrace(new LogonTraceRepository(file.FullName))).ToList();
            return result;
        }
    }
}