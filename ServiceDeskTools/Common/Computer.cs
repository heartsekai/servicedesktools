﻿using System;
using System.Net.NetworkInformation;

namespace ServiceDeskTools.Common
{
    public class Computer
    {
        public string ComputerName { get; set; }

        public Computer(string computerName)
        {
            ComputerName = computerName;
        }

        public bool IsOnline()
        {
            try
            {

            var ping = new Ping();

            PingReply pingReply = ping.Send(ComputerName);

            if (pingReply != null)
                return pingReply.Status == IPStatus.Success;
            }
            catch (Exception e)
            {
            }

            return false;
        }
    }
}