﻿using System;

namespace ServiceDeskTools.Suva
{
    public class LogonTrace
    {
        private string _file;

        public LogonTrace(string fileName)
        {
            _file = fileName;
        }

        public LogonTrace(ILogonTraceRepository logonTraceRepository)
        {
            ComputerName = logonTraceRepository.GetComputerName();
            UserName = logonTraceRepository.GetUserName();
            Action = logonTraceRepository.GetAction();
            Time = logonTraceRepository.GetTime();
            Source = logonTraceRepository.GetFileName();
        }

        public string Source { get; set; }

        public DateTime Time { get; set; }

        public string Action { get; set; }

        public string UserName { get; set; }

        public string ComputerName { get; set; }

        public override bool Equals(object obj)
        {
            return this.Action == (obj as LogonTrace)?.Action &&
                   this.Source == (obj as LogonTrace)?.Source &&
                   this.Time == (obj as LogonTrace)?.Time &&
                   this.UserName == (obj as LogonTrace).UserName &&
                   this.ComputerName == (obj as LogonTrace).ComputerName;
        }
    }
}