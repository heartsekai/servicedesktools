﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ServiceDeskTools.Suva
{
    public class LogonTraceRepository : ILogonTraceRepository
    {
        private string _file;

        public LogonTraceRepository(string fileName)
        {
            _file = fileName;
        }

        public override bool Equals(object obj)
        {
            return this._file == (obj as LogonTraceRepository)._file;
        }

        public string GetComputerName()
        {
            string computerName = "";
            using (StreamReader streamReader = new StreamReader(_file))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (line.Contains("Computer Name"))
                    {
                        computerName = line.Split(';')[1].Trim();
                    } 
                }
            }

            return computerName;
        }

        public string GetUserName()
        {
            return Path.GetFileName(_file).Split('_')[0];
        }

        public string GetAction()
        {
            return Path.GetFileNameWithoutExtension(_file).Split('_').Last();
        }

        public DateTime GetTime()
        {
            string time = "";
            using (StreamReader streamReader = new StreamReader(_file))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (line.Contains("Logon started") || line.Contains("Logoff started"))
                    {
                        time = line.Split(';').Last().Trim();
                        break;
                    }
                }
            }

            if (String.IsNullOrEmpty(time))
                return new DateTime();
            DateTime formatedTime;

            var formatTime = new string[] {"dd.M.yyyy HH:mm:ss", "M/dd/yyyy hh:mm:ss tt"};
            
            if (DateTime.TryParseExact(time, formatTime, CultureInfo.InvariantCulture, DateTimeStyles.None,
                out formatedTime))
            {
                return formatedTime;
            }
                return new DateTime();
        }

        public string GetFileName()
        {
            return Path.GetFileNameWithoutExtension(_file);
        }
    }
}