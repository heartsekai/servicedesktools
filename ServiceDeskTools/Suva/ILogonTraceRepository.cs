﻿using System;

namespace ServiceDeskTools.Suva
{
    public interface ILogonTraceRepository
    {
        bool Equals(object obj);
        string GetComputerName();
        string GetUserName();
        string GetAction();
        DateTime GetTime();
        string GetFileName();
    }
}