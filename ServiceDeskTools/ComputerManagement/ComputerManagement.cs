﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.Linq;

namespace ServiceDeskTools.ComputerManagement
{
    public class ComputerManagement
    {
        private string computerName;
        public List<Principal> GetMembersFromGroup(string groupName)
        {
            var groupContext = new PrincipalContext(ContextType.Machine,computerName);
            var group = GroupPrincipal.FindByIdentity(groupContext, groupName);
            
            var search = new PrincipalSearcher(group);
            return search.FindAll().ToList();
        }
        public string NsLookUp()
                {
                    // https://www.tek-tips.com/viewthread.cfm?qid=1682014
                    Process process = new Process();
                    ProcessStartInfo processStartInfo = new ProcessStartInfo();
                    processStartInfo.FileName = "nslookup.exe";
                    processStartInfo.Arguments = computerName;
                    processStartInfo.RedirectStandardOutput = true;
                    processStartInfo.UseShellExecute = false;
        
                    processStartInfo.CreateNoWindow = false;
                    process.StartInfo = processStartInfo;
                    process.Start();
        
                    process.WaitForExit();
        
                    return process.StandardOutput.ReadToEnd();
                }
    }
}