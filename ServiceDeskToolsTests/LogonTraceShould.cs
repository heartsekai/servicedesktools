using NUnit.Framework;
using ServiceDeskTools.Suva;

namespace ServiceDeskToolsTests
{
    public class LogonTraceShould
    {
        private string _fileName;
        private string _logonTraceFolder;

        [OneTimeSetUp]
        public void Run()
        {
            _logonTraceFolder = @"C:\temp\LogonTraces";
            _fileName = @"C:\temp\LogonTraces\a1k_fc_Logon.txt";
        }
        
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Get_ComputerName_From_FileName()
        {
            LogonTrace logonTrace = new LogonTrace(_fileName);

            string actual = logonTrace.ComputerName;
            
            Assert.That(actual, Is.EqualTo("C810129"));
        }

        [Test]
        public void Get_UserName_From_FileName()
        {
            LogonTrace logonTrace = new LogonTrace(_fileName);

            string actual = logonTrace.UserName.ToUpper();
            
            Assert.That(actual, Is.EqualTo("A1K"));
        }

    }
}