﻿using System;
using System.Globalization;
using System.Linq;
using System.Management.Automation;
using NUnit.Framework;
using PSServiceDeskTools;

namespace ServiceDeskToolsTests.PSServiceDeskTools
{
    public class GetLogonTraceShould
    {
        [Test]
        public void Get_ComputerName_On_User()
        {
            var cmdlet = new GetLogonTrace {UserName = "A1K"};

            var result = cmdlet.Invoke<PSObject>();
            var actual = result.First();
            
            //Assert.That(actual.Count, Is.EqualTo(2));
            Assert.That(actual.Properties["Action"].Value, Is.EqualTo("Logon"));
            Assert.That(actual.Properties["ComputerName"].Value, Is.EqualTo("C810129"));
            Assert.That(actual.Properties["Time"].Value, Is.EqualTo(DateTime.ParseExact("17.10.2019 08:31:19", "d.M.yyyy hh:mm:ss", CultureInfo.InvariantCulture)));
            Assert.That(actual.Properties["FileName"].Value, Is.EqualTo("a1k_fc_Logon"));
        }

        [Test]
        public void Get_UserName_On_Computer()
        {
            var cmdlet = new GetLogonTrace() {ComputerName = "C810129"};

            var actual = cmdlet.Invoke<PSObject>().ToList();

            Assert.That(actual[0].Properties["Action"].Value, Is.EqualTo("Logon"));
        }
    }
}