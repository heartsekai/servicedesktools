﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PSServiceDeskTools;

namespace ServiceDeskToolsTests.PSServiceDeskTools
{
    public class GetColumbusLogShould
    {
        [Test]
        public void Get_Columbus_Logfile()
        {
           var cmdlet = new GetColumbusLog {ComputerName = "C040143"};

           IEnumerable<string> result = cmdlet.Invoke<string>();
           var actual = result.First();
           
           Assert.That(actual, Is.EqualTo(@"\\C040143\c$\Windows\Brainware.log"));
        }

        [Test]
        public void Get_Columbus_Logfile_From_Current_Computer_When_No_Parameters()
        {
           var cmdlet = new GetColumbusLog();

           var result = cmdlet.Invoke<string>();
           var actual = result.First();
           
           Assert.That(actual, Is.EqualTo(@"c:\Windows\Brainware.log"));
        }
        
    }
}