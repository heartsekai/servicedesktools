﻿using System.Collections.Generic;
using NUnit.Framework;
using ServiceDeskTools.Common;
using ServiceDeskTools.Suva;

namespace ServiceDeskToolsTests
{
    public class FileFinderShould
    {
        private string _logonTraceFolder;

        [OneTimeSetUp]
        public void Run()
        {
            _logonTraceFolder = @"C:\temp\LogonTraces";
        }
        
        [Test]
        public void Get_List_Of_LogonTraces_From_Given_User()
        {
            LogonTraceFinder logonTraceFinder = new LogonTraceFinder(_logonTraceFolder);
            
            List<LogonTrace> actual = logonTraceFinder.GetLogonTracesFrom("a1m");

            var expected = new List<LogonTrace>()
            {
                new LogonTrace(@"C:\temp\LogonTraces\a1m_fc_Logon.txt")
            };
            
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void Get_List_Of_Two_LogonTraces_From_Given_User()
        {
            LogonTraceFinder logonTraceFinder = new LogonTraceFinder(_logonTraceFolder);
            
            List<LogonTrace> actual = logonTraceFinder.GetLogonTracesFrom("a1k");

            var expected = new List<LogonTrace>()
            {
                new LogonTrace(@"C:\temp\LogonTraces\a1k_fc_Logon.txt"),
                new LogonTrace(@"C:\temp\LogonTraces\a1k_vm_Logon.txt")
            };
            
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}