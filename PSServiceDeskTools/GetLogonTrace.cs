﻿using System.Collections.Generic;
using System.Management.Automation;
using ServiceDeskTools;
using ServiceDeskTools.Suva;

namespace PSServiceDeskTools
{
    [Cmdlet(VerbsCommon.Get, "LogonTrace")]
    public class GetLogonTrace : Cmdlet
    {
        [Parameter(ParameterSetName = "User")] public string UserName { get; set; }

        [Parameter(ParameterSetName = "Computer")]
        public string ComputerName { get; set; }

        [Parameter] public string Path { get; set; } = @"\\suvanet.ch\dfsroot\transfer\SpezData\LOGONTRACE";

        protected override void ProcessRecord()
        {
            GetLogonTraceUseCase getLogonTraceUseCase = new GetLogonTraceUseCase();

            List<LogonTrace> traces;

            if (!string.IsNullOrEmpty(UserName))
            {
                traces = getLogonTraceUseCase.GetLogonTracesFromUser(Path, UserName);
            }
            else
            {
                traces = getLogonTraceUseCase.GetLogonTracesFromComputer(Path, ComputerName);
            }

            Print(traces);
        }

        private void Print(List<LogonTrace> traces)
        {
            foreach (var trace in traces)
            {
                WriteObject(LogonTraceToPsObjectConverter(trace));
            }
        }

        private PSObject LogonTraceToPsObjectConverter(LogonTrace trace)
        {
            var converter = new PSObject();
            converter.Properties.Add(
                new PSVariableProperty(
                    new PSVariable("Action", trace.Action))
            );
            converter.Properties.Add(
                new PSVariableProperty(
                    new PSVariable("ComputerName", trace.ComputerName))
            );
            converter.Properties.Add(
                new PSVariableProperty(
                    new PSVariable("Time", trace.Time))
            );
            converter.Properties.Add(
                new PSVariableProperty(
                    new PSVariable("FileName", trace.Source))
            );

            return converter;
        }
    }
}