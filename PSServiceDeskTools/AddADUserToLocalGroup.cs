﻿using System;
using System.Management.Automation;
using ServiceDeskTools.Common;

namespace PSServiceDeskTools
{
    [Cmdlet(VerbsCommon.Add, "ADUserToLocalGroup")]
    public class AddAdUserToLocalGroup : Cmdlet
    {
        [Parameter()]
        public string ComputerName { get; set; }

        [Parameter()]
        public string UserName { get; set; }


        [Parameter()]
        public string GroupName { get; set; } = @"Remote Desktop Users";
        
        protected override void ProcessRecord()
        {
            var computer = new Computer(ComputerName);
            if (!computer.IsOnline())
            {
                throw new Exception($"{ComputerName} is Offline.");
            }

            var userManagement = new UserManagement();

            var result = userManagement.AddUserToLocalGroup(UserName, GroupName, ComputerName);

            Print(result);
        }

        private void Print(bool result)
        {
            WriteObject(result);
        }
    }
}