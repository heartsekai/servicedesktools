﻿using System;
using System.Management.Automation;
using ServiceDeskTools.Common;

namespace PSServiceDeskTools
{
    [Cmdlet(VerbsCommon.Remove, "WindowsProfile")]
    public class RemoveWindowsProfile : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 1)]
        [ValidateNotNullOrEmpty]
        public string ComputerName { get; set; } = Environment.MachineName;

        [Parameter(Mandatory = true, Position = 0)]
        [ValidateNotNullOrEmpty]
        public string UserName { get; set; }

        protected override void ProcessRecord()
        {
            var computer = new Computer(ComputerName);
            if (!computer.IsOnline())
                throw new Exception($"{ComputerName} is Offline.");
            
            var userManagement = new UserManagement();

            WriteObject(userManagement.DeleteProfile(UserName,ComputerName));
        }
    }
}