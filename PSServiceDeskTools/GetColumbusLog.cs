﻿using System.Management.Automation;

namespace PSServiceDeskTools
{
    [Cmdlet(VerbsCommon.Get, "ColumbusLog")]
    public class GetColumbusLog : Cmdlet
    {
        [Parameter] public string ComputerName { get; set; }

        protected override void ProcessRecord()
        {
            //GetColumbusLogUseCase = new GetColumbusLogUseCase();
            Print(!string.IsNullOrEmpty(ComputerName)
                ? $"\\\\{ComputerName}\\c$\\Windows\\Brainware.log"
                : @"c:\Windows\Brainware.log");
        }

        private void Print(string output)
        {
            WriteObject(output);
        }
    }
}