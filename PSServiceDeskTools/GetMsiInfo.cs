﻿using System.Management.Automation;
using System.Security.Principal;

namespace PSServiceDeskTools
{
    [Cmdlet(VerbsCommon.Get, "MSIInfo")]
    public class GetMsiInfo : Cmdlet
    {
        [Parameter(Mandatory = true)]
        [ValidateNotNullOrEmpty]
        public string Path;

        protected override void ProcessRecord()
        {
            
        }
    }
}