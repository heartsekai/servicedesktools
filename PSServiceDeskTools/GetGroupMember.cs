﻿using System;
using System.Management.Automation;
using ServiceDeskTools;
using ServiceDeskTools.Common;
using ServiceDeskTools.ComputerManagement;

namespace PSServiceDeskTools
{
    [Cmdlet(VerbsCommon.Get, "GroupMember")]
    public class GetGroupMember : Cmdlet
    {
        [Parameter(Mandatory = true)]
        public string GroupName;

        [Parameter] public string ComputerName;
        
        protected override void ProcessRecord()
        {
            var computer = new Computer(ComputerName);
            if (!computer.IsOnline())
                throw new Exception($"{ComputerName} is Offline.");

            var computerManagement = new ComputerManagement();

            var members = computerManagement.GetMembersFromGroup(GroupName);
            WriteObject(members);
        }
    }
}