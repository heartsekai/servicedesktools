﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using ServiceDeskTools.Common;

namespace MyServiceDesk
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private MainWindowController Model = new MainWindowController();
        public MainWindow()
        {
            InitializeComponent();

            ConnectionStatus.DataContext = Model;
            ComputerNameTextBox.DataContext = Model;
            NsLookUpTextBox.DataContext = Model;

        }

        private void ComputerNameTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Computer computer = new Computer((sender as TextBox)?.Text);
                Model.ConnectionStatusColor = computer.IsOnline() ? Brushes.Chartreuse : Brushes.DarkOrange;
            }
        }
    }
}