﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using MyServiceDesk.Annotations;

namespace MyServiceDesk
{
    internal class MainWindowController : BindableBase
    {
        private SolidColorBrush _connectionStatusColor = Brushes.DarkOrange;

        public SolidColorBrush ConnectionStatusColor
        {
            get => _connectionStatusColor;
            set
            {
                if (value != _connectionStatusColor)
                {
                    _connectionStatusColor = value;
                    OnPropertyChanged();
                }
            } 
        }

        private string _nsLookUp;
        public string NsLookUp { 
            get => _nsLookUp;
            set
            {
                if (value != _nsLookUp)
                {
                    _nsLookUp = value;
                    OnPropertyChanged();
                }
            }
        }

        public string ComputerName;

    }

    internal class BindableBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}